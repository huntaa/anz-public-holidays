using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using System.Data.SqlClient;


namespace btg_project {
    class HolidaysContext: DbContext {
        public DbSet<ANZPublicHolidays>? ANZPublicHolidays {get; set;}
        protected override void OnConfiguring(DbContextOptionsBuilder options) {

            options.UseSqlServer("Server=localhost;Database=Holidays;User Id=sa;Password=Password123123;");
        }
    }

    public class ANZPublicHolidays {
        public int ID {get; set;}
        public string? HolidayName {get; set;}
        public string? CountryName {get; set;}
        public DateTime Date {get; set;}
    }
}