#!/bin/bash
cp btg_project.csproj Image/
cp Program.cs Image/
cp Database.cs Image/
sudo docker container stop holiday_container
sudo docker container rm holiday_container
sudo docker build --tag holiday_image Image/
sudo docker run -e "ACCEPT_EULA=Y" -e "SA_PASSWORD=Password123123" -e "MSSQL_PID=Express" -p 1433:1433 --name "holiday_container" -d holiday_image
rm Image/btg_project.csproj
rm Image/Program.cs
rm Image/Database.cs
