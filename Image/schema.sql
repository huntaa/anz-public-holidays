CREATE DATABASE Holidays;
GO
USE Holidays;
GO
CREATE TABLE ANZPublicHolidays (
    ID int identity(1,1), 
    HolidayName nvarchar(256),
    CountryName nvarchar(256),
    Date Date
);
GO
ALTER TABLE ANZPublicHolidays
ADD PRIMARY KEY (ID); 
GO