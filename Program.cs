﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.SqlServer;
using System.Data.SqlClient;

namespace btg_project {
    class HolidayApp {
        public static void Main() {
            Console.WriteLine("Starting...");
            HolidaysContext Db = new HolidaysContext();

            Db.Add(
                new ANZPublicHolidays {
                    HolidayName = "Christmas",
                    CountryName = "New Zealand",
                    Date = new DateTime()
                }
            );
            Db.SaveChanges();
            Console.WriteLine("All done.");
        }
    }
}